def test_unit():
    import pykdgrav3_utils.units as u

    unit = u.units(1e10, 1.0)

    # Very simple for illustrative purposes
    assert type(unit) == u.units
