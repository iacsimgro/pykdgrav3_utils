import numpy as np
import pytest

import pykdgrav3_utils.timings as t


@pytest.fixture
def data():
    return t.Timing("./tests/data/test.timing")


def test_load(data):
    assert type(data) == t.Timing


def test_len(data):
    assert len(data) == 3


def test_runtype(data):
    assert type(data[0]) == t.TimingRun


def test_emptyruns(data):
    with pytest.raises(Exception) as excinfo:
        data[1]
    assert str(excinfo.value) == "Trying to load an empty run!"


def test_outofbounds(data):
    with pytest.raises(Exception) as excinfo:
        data[10]
    assert type(excinfo.value) == KeyError


def test_rundata(data):
    assert type(data[0].get_data()) == np.ndarray

    assert data[0].get_names() == (
        "Step",
        "Gravity",
        "IO",
        "Tree",
        "DomainDecom",
        "KickOpen",
        "KickClose",
        "Density",
        "EndTimeStep",
        "Gradient",
        "Flux",
        "TimeStep",
        "Drift",
        "FoF",
        "Feedback",
        "StarForm",
        "BHs",
        "Stev",
        "Others",
    )


def test_plot(data):
    data[0].plot(save="tests/data/test1.png")
    data[0].plot(acc=True, save="tests/data/test2.png")
    data[0].plot(fields=["Gravity", "Flux"], save="tests/data/test3.png")
